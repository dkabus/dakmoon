#ifndef BITMAP_H
#define BITMAP_H

#include <stdio.h>
#include <math.h>

void draw_moon(float phase_in_revolutions, int n, char light, char shadow, char space_light, char space_shadow, const char* newline);

#endif
