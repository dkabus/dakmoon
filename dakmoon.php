<?php
$lang = "en";
if (isset($_GET["lang"])) { $lang = $_GET["lang"]; }

$args = "";
if (isset($_GET["phase"])) {
    try {
        $args = "-l " . number_format($_GET["phase"], "4");
    } catch (Exception $e) {
        print "Can not convert a string to a number.";
    }
}

$keys = array("1","c","d","e","E","i","r","s","t","T","y");
exec("dakmoon $args -".join("n", $keys)."n", $values, $retval);
$moon = array();
foreach ($values as $k => $v) { $moon[$keys[$k]] = $v; }

if ($lang == "de") {
    $str = array();
    $str["t"] = $moon["T"];
    $str["d"] = "Phase in Grad";
    $str["c"] = "Phase in Prozent";
    $str["r"] = "Phase in Bogenmaß";
    $str["y"] = "Tag der Phase";
}
else { // en
    $str = array();
    $str["t"] = $moon["t"];
    $str["d"] = "phase in degrees";
    $str["c"] = "phase in percent";
    $str["r"] = "phase in radians";
    $str["y"] = "day in phase";
}

?>
<html>

<head>
    <link rel="icon" href="img/icon-<?php echo $moon["i"]; ?>.ico" type="image/png" />
    <title><?php echo $moon["d"]; ?> DAK-Moon</title>
    <meta http-equiv="refresh" content="60" />
    <style>
        body {
            background: #444;
            color: white;
            font-family: sans-serif;
            margin: 0;
            padding: 0;
        }
        table {
            background: black;
            margin: 0 auto;
            max-width: 50em;
            width: 100%;
            overflow: hidden;
        }
        img {
            margin: 2em;
        }
        a.lang {
            border-color: #999;
        }
        a {
            display: inline-block;
            float: right;
            padding: 0.2em;
            margin: 0.2em;
            color: #999;
            text-decoration: none;
            border: 0.1em solid black;
        }
        a:hover {
            color: white;
            border-color: white;
        }
        td {
            vertical-align: middle;
        }
        .main {
            width: 100%;
        }
    </style>
</head>

<body>
<table>
<tr>
<td>
<img src="img/photo.<?php echo sprintf("%02s", substr($moon["c"], 0, -1)); ?>.png" />
</td>
<td class="main">
<h1><?php echo $moon["e"]; ?> <?php echo $str["t"]; ?></h1>
<ul>
<li><?php echo $str["d"]; ?>: <?php echo $moon["d"]; ?></li>
<li><?php echo $str["c"]; ?>: <?php echo $moon["c"]; ?></li>
<li><?php echo $str["r"]; ?>: <?php echo $moon["r"]; ?></li>
<li><?php echo $str["y"]; ?>: <?php echo $moon["y"]; ?></li>
</ul>
<a class="lang" href="?lang=en">en</a>
<a class="lang" href="?lang=de">de</a>
<a href="README.md">README</a>
</td>
</tr>
</table>
</body>

</html>
