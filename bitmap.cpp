#include "bitmap.h"

// Function to draw the moon on a square grid of size n
void draw_moon(float phase, int n, char light, char shadow, char space_light, char space_shadow, const char* newline)
{
    phase = fmod(phase, 1.0f);

    // Different colour for both sides of space
    char space_left = phase < 0.5 ? space_shadow : space_light;
    char space_right = phase < 0.5 ? space_light : space_shadow;

    // convert phase to radians with the correct offset
    phase = (phase + 0.25)*2.0f*M_PI;

    // Calculate the mid of the grid
    int mid = n / 2;

    // Loop through the rows of the grid
    for (int i = 0; i < n; i++)
    {
        // Loop through the columns of the grid
        for (int j = 0; j < n; j++)
        {
            // Calculate the 3D point on the moon's surface
            float x = float(j - mid) / mid;
            float y = float(i - mid) / mid;
            float r = sqrt(x*x + y*y);
            float z = sqrt(1 - r*r);

            // Calculate the angle to the sun
            float angle = atan2(z, x);

            // Calculate the angular difference between angle and phase
            float diff = angle - phase;
            if (diff < -M_PI) { diff += 2.0f*M_PI; }
            if (diff >  M_PI) { diff -= 2.0f*M_PI; }
            diff = fabs(diff);

            if (r < 1) {
                if (diff < M_PI / 2.0f)
                { printf("%c", shadow); }
                else
                { printf("%c", light); }
            }
            else
            {
                if (x < 0) {
                    printf("%c", space_left);
                } else {
                    printf("%c", space_right);
                }
            }
        }
        // Move to the next line after each row
        printf(newline);
    }
}
