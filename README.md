# DAK-Moon

A small program to calculate the current moon phase.

- `dakmoon`: calculations and outputs in different units
- `dakmoon-ascii`: present the moon phase in the terminal using ASCII art, requires [`jp2a`](https://github.com/Talinx/jp2a/), example output below
- `dakmoon-ascii-de`: same thing but in German
- `dakmoon.php`: present the moon phase as a website using images, run a development server to preview via `php -S 127.0.0.1:8000`
- `img/`: this folder contains another `Makefile` to create the icons and images for the website

```
         .'',;k0d,
     ..,,,,,,,;0MMWOc
   .,,,,,,,,,,,;NMMMMWx.
 .,,,,,,,,,,,,,,xMMMMMMW:     🌓 First Quarter Moon
 ,,,,,,,,,,,,,,,cMMMMMMMM;
',,,,,,,,,,,,,,,;WMMMMMMMX    Phase in degrees: 72°
,,,,,,,,,,,,,,,,,WMMMMMMMM    Phase in radians: 1.25664
',,,,,,,,,,,,,,,;WMMMMMMMX    Phase in percent: 20%
 ,,,,,,,,,,,,,,,cMMMMMMMM;    Day in phase: 5.906
 .,,,,,,,,,,,,,,xMMMMMMW:
   .,,,,,,,,,,,;NMMMMWx.
     ..,,,,,,,;0MMWOc
         .',,:0Kx,
```

## Dependencies:

- GNU C++ compiler
- GNU make
- GNU coreutils
- sed
- [`jp2a`](https://github.com/Talinx/jp2a/) (optional)
- [ImageMagick](https://imagemagick.org/) (optional)

## Image source:

- `img/photo.png`: Gregory H. Revera, licensed under CC BY-SA 3.0, https://en.wikipedia.org/wiki/File:FullMoon2010.jpg
- `img/icon.light.svg`: Twemoji by twitter, licensed under CC BY 4.0, https://github.com/twitter/twemoji
- `img/icon.shadow.svg`: Twemoji by twitter, licensed under CC BY 4.0, https://github.com/twitter/twemoji
