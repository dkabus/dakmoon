#include <cstdarg>
#include <cstdio>
#include <iostream>
#include <stdlib.h>

static char* argv0;
#include "arg.h"
#include "bitmap.h"
#include "map.h"
#include "moon.h"

str get(char c, const MoonPhase& m) {
  MapInfo i = get_map_info(c);
  if( i.index_by == "boundary" )
    return str{i.map.closest_at(m.boundary())};
  else if( i.index_by == "revolutions" )
    return str{i.map.closest_at(m.angle().revolutions())};
  throw "No map found for this index";
}

void die(const char *errstr, ...) {
  va_list ap;

  va_start(ap, errstr);
  vfprintf(stderr, errstr, ap);
  va_end(ap);
  exit(1);
}

void usage(void) {
  die("usage: %s [-1cdeEinrstTy] [-b pixelsize] [-l float] [-p string]\n", argv0);
}

int main(int argc, char *argv[]) {
  MoonPhase m;
  Angle phi = m.angle();
  if( argc == 1 )
    std::cout << get('e', m) << " DAK-Moon " << int(phi.degrees()) << "°" << std::endl;

  int size = 0;
  ARGBEGIN {
    case '1':
      std::cout << phi.revolutions(); break;
    case 'b':
      size = std::stoi(EARGF(usage()));
      printf("P5 %d %d 3\n", size, size);
      draw_moon(phi.revolutions(), size, 3, 2, 1, 0, ""); break;
    case 'c':
      std::cout << int(phi.revolutions()*100) << "%"; break;
    case 'd':
      std::cout << int(phi.degrees()) << "°"; break;
    case 'l':
      phi = Angle(std::stof(EARGF(usage())));
      m = MoonPhase(phi);
      break;
    case 'n':
      std::cout << std::endl; break;
    case 'r':
      std::cout << phi.radians(); break;
    case 's':
      std::cout << " "; break;
    case 'p':
      std::cout << EARGF(usage()); break;
    case 'y':
      std::cout << phi.revolutions()*SynodMonth/24/60/60; break;
    case 'e':
    case 'E':
    case 'i':
    case 't':
    case 'T':
      std::cout << get(argc_, m); break;
    default:
      usage();
  } ARGEND;
  return 0;
}
