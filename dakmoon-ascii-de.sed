#!/bin/sed -f
s/ -t\>/ -T/g;
s/\<Day in phase\>/Tag der Phase/g;
s/\<phase\>/Phase/g;
s/\<degrees\>/Grad/g;
s/\<percent\>/Prozent/g;
s/\<radians\>/Bogenmaß/g
