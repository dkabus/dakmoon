#ifndef MAP_H
#define MAP_H

#include <map>
#include <string>
#include <vector>

typedef std::string str;

class Map : public std::map<float,str> {
  public:
    const_iterator closest(const key_type& k) const
    {
      const_iterator it = this->lower_bound(k);

      if( it == this->end() )
	return --it;

      if( it == this->begin() )
	return it;

      key_type diff = it->first - k;
      it--;
      if( diff < k - it->first )
	it++;
      return it;
    }
    const mapped_type& closest_at(const key_type& k) const
    { return this->closest(k)->second; }
};

class MapInfo {
  public:
    Map map;
    str index_by;
    MapInfo(Map map={}, str index_by="revolutions") : map(map) , index_by(index_by) {}
};

MapInfo get_map_info(char name);

#endif
