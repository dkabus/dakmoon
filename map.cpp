#include "map.h"

Map homogeneous(std::vector<str> list, double max=1.)
{
  Map map;
  for(int i=0; i<list.size(); i++)
    map[double(i)*max/list.size()] = list[i];
  map[max] = list[0];
  return map;
}

str icon_name(bool inc, int d)
{
  char s[9];
  snprintf(s, sizeof(s), "%c%02d", inc ? 'i' : 'd', d);
  return s;
}

MapInfo get_map_info(char name)
{
  switch(name) {
    case 'e':
      return homogeneous({"🌑", "🌒", "🌓", "🌔", "🌕", "🌖", "🌗", "🌘"});
    case 'E':
      return homogeneous({"🌚","🌛","🌝","🌜"});
    case 't':
      return homogeneous({
        "New Moon",
        "Waxing Crescent Moon",
        "First Quarter Moon",
        "Waxing Gibbous Moon",
        "Full Moon",
        "Waning Gibbous Moon",
        "Last Quarter Moon",
        "Waning Crescent Moon",
      });
    case 'T':
      return homogeneous({
        "Neumond",
        "zunehmende Sichel",
        "zunehmender Halbmond",
        "zunehmender Mond",
        "Vollmond",
        "abnehmender Mond",
        "abnehmender Halbmond",
        "abnehmende Sichel",
      });
    case 'i':
      // fuellt das Icon-Handle-Array
      std::vector<str> v;
      v.push_back(icon_name(false, 32));
      for( int i=1; i<=32; i++)
	      v.push_back(icon_name(true, i));
      for( int i=1; i<=31; i++)
	      v.push_back(icon_name(false, i));
      return {homogeneous(v, 2.), "boundary"};
  }
  return {};
}
