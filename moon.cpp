#include "moon.h"

Angle MoonPhase::angle() const
{
  // Winkel der Tag-Nacht-Grenze auf dem Mond berechnen
  long secs = this->secs-FirstNewMoon;
  long secs_from_newmoon=secs-(secs/SynodMonth)*SynodMonth;
  return Angle{(double)secs_from_newmoon/(double)SynodMonth};
}

double MoonPhase::boundary() const
{
  // Berechne, über wo die Tag-Nacht-Grenze auf dem Mond verläuft
  // Neumond: 0, zunehmend, Vollmond: 1, abnehmend, wieder Neumond: 2
  // Werte größer als 1 liegen auf der Rückseite
  const double phi_rad = this->angle().radians();
  double a;
  if(phi_rad < M_PI)
    a=1.0-cos(phi_rad);
  else
    a=3.0+cos(phi_rad);
  return 0.5*a;
}
