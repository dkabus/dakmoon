SOURCE=$(wildcard *.h) $(wildcard *.cpp)

all: dakmoon dakmoon-ascii-de

dakmoon: $(SOURCE)
	g++ $^ -o $@

dakmoon-ascii-de: dakmoon-ascii-de.sed dakmoon-ascii
	sed -f $^ > $@
	chmod +x $@

clean:
	rm -f dakmoon dakmoon-ascii-de

test: dakmoon
	test "$$(./$< -l 0.1 -1scsdsesEsisrsystsTn)" = "0.1 10% 36° 🌒 🌚 i03 0.628319 2.953 Waxing Crescent Moon zunehmende Sichel" && echo "Test successful."

install: dakmoon dakmoon-ascii-de
	install -Dm755 -t $(DESTDIR)$(PREFIX)/bin/ dakmoon
	install -Dm755 -t $(DESTDIR)$(PREFIX)/bin/ dakmoon-ascii
	install -Dm755 -t $(DESTDIR)$(PREFIX)/bin/ dakmoon-ascii-de
	#install -Dm644 -t $(DESTDIR)$(MANPREFIX)/man1/ dakmoon.1
	#install -Dm644 -t $(DESTDIR)$(PREFIX)/share/applications/ dakmoon.desktop

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dakmoon
	rm -f $(DESTDIR)$(PREFIX)/bin/dakmoon-ascii
	rm -f $(DESTDIR)$(PREFIX)/bin/dakmoon-ascii-de
	# rm -f $(DESTDIR)$(MANPREFIX)/man1/dakmoon.1
	# rm -f $(DESTDIR)$(PREFIX)/share/applications/dakmoon.desktop
