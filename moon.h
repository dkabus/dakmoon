/***********************************************************************
* Autoren......: Dipl.-Ing. Diethelm Albert Kabus, Desmond Albert Kabus M.Sc.
* Datum........: 09.09.1995, 13.09.1995, 03.10.1995, 20.03.2002, 04.04.2020
* Beschreibung.: Darstellung der Mondphase als Icon
***********************************************************************/

#ifndef MOON_H
#define MOON_H
#include <time.h>
#include <math.h>
#include <stdio.h>

#ifndef M_PI // falls M_PI noch nicht
#define M_PI 3.14159265358979323846 // definiert ist, definieren
#endif

static const long FirstNewMoon = 804304200L; // 28.06.95 2.50 MESZ
static const long SynodMonth = 2551392L; // Synodischer Monat

class Angle {
  public:
    double value; // in Umdrehungen
    Angle(double revolutions=0.) : value(fmod(revolutions, 1.0f)) {}
    double revolutions(bool symmetric=false) const
    { return (symmetric and (this->value > 0.5) ) ? this->value - 1. : this->value; }
    double radians(bool symmetric=false) const { return this->revolutions(symmetric)*2.*M_PI; }
    double degrees(bool symmetric=false) const { return this->revolutions(symmetric)*360.; }
};

class MoonPhase {
  public:
    time_t secs;
    MoonPhase() : secs(time(NULL)) {}
    MoonPhase(const time_t& time) : secs(time) {}
    MoonPhase(const Angle& phase) : secs(FirstNewMoon + phase.revolutions()*SynodMonth) {}
    Angle angle() const;
    double boundary() const;
};

#endif
